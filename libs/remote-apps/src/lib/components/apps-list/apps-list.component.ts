import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { RemoteApp } from '@ods/commons';

@Component({
  selector: 'ods-apps-list',
  templateUrl: './apps-list.component.html',
  styleUrls: ['./apps-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppsListComponent {
  @Input() appsList: RemoteApp[] | null = null;
}
