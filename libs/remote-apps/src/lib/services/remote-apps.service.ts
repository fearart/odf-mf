import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { shareReplay, pluck, catchError } from 'rxjs/operators';

import { AppConfig, APP_CONFIG, RemoteApp } from '@ods/commons';

import { RemoteAppsResponse } from './../models/remote-apps.model';
import { PARAMS_DEFAULTS } from './../models/default.const';

@Injectable({ providedIn: 'root' })
export class RemoteAppsService {
  readonly remoteApps$: Observable<RemoteApp[]>;

  constructor(
    private readonly _http: HttpClient,
    @Inject(APP_CONFIG) appConfig: AppConfig
  ) {
    const params$ = this._http
      .get<RemoteAppsResponse>(
        `${appConfig.proxyUrl}/api/v1/${appConfig.appName}/configuration`
      )
      .pipe(
        shareReplay<RemoteAppsResponse>(1),
        catchError(() => of(PARAMS_DEFAULTS))
      );

    this.remoteApps$ = params$.pipe(pluck('remoteApps'));
  }
}
