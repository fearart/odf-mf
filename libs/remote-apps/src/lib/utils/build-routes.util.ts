import { loadRemoteModule } from '@angular-architects/module-federation';
import { Routes } from '@angular/router';
import * as R from 'ramda';

import { RemoteApp } from '@ods/commons';

export const buildRoutes = (remoteApps: RemoteApp[]): Routes =>
  remoteApps.map(app => ({
    path: app.routePath,
    loadChildren: () => loadRemoteModule(app).then(m => m[app.ngModuleName])
  }));

export const addChildernRoutes = R.curry(
  (childrenRoutes: Routes, key: string, routes: Routes) =>
    R.map(
      R.when(R.propEq('path', key), R.assoc('children', childrenRoutes)),
      routes
    )
);
