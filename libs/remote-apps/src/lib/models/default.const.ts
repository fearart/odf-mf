import { RemoteAppsResponse } from './remote-apps.model';
export const PARAMS_DEFAULTS: RemoteAppsResponse = {
  remoteApps: [
    {
      remoteEntry: 'http://localhost:5000/remoteEntry.js',
      remoteName: 'patientJourney',
      exposedModule: './PyModule',
      displayName: 'Patient Journey',
      routePath: 'patient-journey',
      ngModuleName: 'FlightsModule'
    }
  ]
};
