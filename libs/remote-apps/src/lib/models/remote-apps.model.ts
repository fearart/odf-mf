import { RemoteApp } from '@ods/commons';
export interface RemoteAppsResponse {
  remoteApps: RemoteApp[];
}
