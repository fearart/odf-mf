import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AppsListComponent } from './components/apps-list/apps-list.component';

import { RemoteAppsService } from './services/remote-apps.service';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [AppsListComponent],
  exports: [AppsListComponent],
  providers: [RemoteAppsService]
})
export class RemoteAppsModule {}
