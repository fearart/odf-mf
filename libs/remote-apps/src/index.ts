export * from './lib/remote-apps.module';
export * from './lib/services/remote-apps.service';
export * from './lib/utils/build-routes.util';
export * from './lib/models/remote-apps.model';
