export interface RemoteApp {
  remoteEntry: string;
  remoteName: string;
  exposedModule: string;
  displayName: string;
  routePath: string;
  ngModuleName: string;
}
