export interface AppConfig {
  proxyUrl: string;
  appName: string;
}
