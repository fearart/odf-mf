const nxPreset = require('@nrwl/jest/preset');
module.exports = {
  ...nxPreset,
  testMatch: ['**/+(*.)+(spec|test).+(ts|js)?(x)'],
  transform: {
    '^.+\\.(ts|js|html)$': 'ts-jest',
    '^.+\\.svg$': 'jest-transform-stub'
  },
  resolver: '@nrwl/jest/plugins/resolver',
  moduleFileExtensions: ['ts', 'js', 'html'],
  collectCoverage: true,
  coverageReporters: ['html', 'lcov']
};

// Handle fail on => In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', (err) => {
  fail(err);
});
// Workaround for Jest timezone issue
// https://stackoverflow.com/questions/56261381/how-do-i-set-a-timezone-in-my-jest-config
process.env.TZ = 'UTC';
