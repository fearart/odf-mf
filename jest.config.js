module.exports = {
  projects: [
    '<rootDir>/apps/ods-mf',
    '<rootDir>/libs/commons',
    '<rootDir>/libs/remote-apps',
    '<rootDir>/libs/remote-apps-launcher',
    '<rootDir>/apps/patient-journey',
    '<rootDir>/libs/styles'
  ]
};
