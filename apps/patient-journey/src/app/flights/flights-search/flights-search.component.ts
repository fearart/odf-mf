import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ods-app-flights-search',
  styleUrls: ['./flights-search.component.scss'],
  templateUrl: './flights-search.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FlightsSearchComponent {
  public from!: string;
  public to!: string;

  public patient$: Observable<string> = this.route.params.pipe(
    map(({ patient }) => patient)
  );

  constructor(private route: ActivatedRoute) {}

  search() {
    alert('Not implemented for this demo!');
  }
}
