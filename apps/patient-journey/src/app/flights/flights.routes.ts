import { Routes } from '@angular/router';

import { FlightsSearchComponent } from './flights-search/flights-search.component';
import { TestComponent } from './components/test/test.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';

export const FLIGHTS_ROUTES: Routes = [
  {
    path: '',
    component: WrapperComponent,
    children: [
      {
        path: 'flight',
        component: FlightsSearchComponent
      },
      {
        path: 'flight/:patient',
        component: FlightsSearchComponent
      },
      {
        path: 'test',
        component: TestComponent
      }
    ]
  }
];
