import { Component } from '@angular/core';

@Component({
  selector: 'ods-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'patient-journey';
}
