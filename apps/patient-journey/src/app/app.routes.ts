import { Routes } from '@angular/router';

export const APP_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'patient-journey'
  },

  {
    path: 'patient-journey',
    loadChildren: () =>
      import('./flights/flights.module').then(m => m.FlightsModule)
  }
];
