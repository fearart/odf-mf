import { Router, Routes } from '@angular/router';
import { map, tap } from 'rxjs/operators';

import { RemoteApp } from '@ods/commons';
import {
  buildRoutes,
  addChildernRoutes,
  RemoteAppsService
} from '@ods/remote-apps';

export const onAppInit = (
  router: Router,
  remoteAppsService: RemoteAppsService
) => () =>
  remoteAppsService.remoteApps$.pipe(
    map((remoteApps: RemoteApp[]) => buildRoutes(remoteApps)),
    tap((remoteAppsRouting: Routes) =>
      router.resetConfig(
        addChildernRoutes(remoteAppsRouting, 'gn', router.config)
      )
    )
  );
