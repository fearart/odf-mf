import { ChangeDetectionStrategy, Component } from '@angular/core';

import { RemoteAppsService } from '@ods/remote-apps';
@Component({
  selector: 'ods-global-navigation',
  templateUrl: './global-navigation.component.html',
  styleUrls: ['./global-navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GlobalNavigationComponent {
  constructor(public remoteAppsService: RemoteAppsService) {}
}
