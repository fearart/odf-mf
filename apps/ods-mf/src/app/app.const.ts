import { AppConfig } from '@ods/commons';

export const APP_CONFIG_LOCALE: AppConfig = {
  proxyUrl: 'http://localhost:3001/proxy-service',
  appName: 'global-navigation'
};
