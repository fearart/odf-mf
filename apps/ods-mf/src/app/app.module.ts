import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { APP_CONFIG } from '@ods/commons';
import { RemoteAppsModule } from '@ods/remote-apps';

import { APP_CONFIG_LOCALE } from './app.const';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { GlobalNavigationComponent } from './views/global-navigation/global-navigation.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    RemoteAppsModule
  ],
  declarations: [AppComponent, GlobalNavigationComponent],
  providers: [{ provide: APP_CONFIG, useValue: APP_CONFIG_LOCALE }],
  bootstrap: [AppComponent]
})
export class AppModule {}
