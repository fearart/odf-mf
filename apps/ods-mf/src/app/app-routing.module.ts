import { NgModule, APP_INITIALIZER } from '@angular/core';
import { Routes, Router, RouterModule } from '@angular/router';

import { RemoteAppsService } from '@ods/remote-apps';

import { onAppInit } from './on-app-init.factory';
import { GlobalNavigationComponent } from './views/global-navigation/global-navigation.component';

export const routes: Routes = [
  {
    path: 'gn',
    component: GlobalNavigationComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'gn'
  },
  {
    path: '**',
    redirectTo: 'gn'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      relativeLinkResolution: 'legacy',
      useHash: true
    })
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: onAppInit,
      deps: [Router, RemoteAppsService],
      multi: true
    }
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
