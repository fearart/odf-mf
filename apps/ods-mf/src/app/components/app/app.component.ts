import { Component } from '@angular/core';

@Component({
  selector: 'ods-root',
  templateUrl: './app.component.html'
})
export class AppComponent {}
